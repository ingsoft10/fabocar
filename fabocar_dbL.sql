﻿# Host: localhost  (Version 5.5.5-10.1.32-MariaDB)
# Date: 2021-10-27 14:49:19
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "cliente"
#

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `correo` varchar(90) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

#
# Data for table "cliente"
#

INSERT INTO `cliente` VALUES (1,'lis','rok',34567,'ggg','fff',0),(24,'LISSS','LISSS',123,'AKJSHD:_:','AFS',0),(25,'LUSHABSHJ UBUH','HJH HJH',345678,'JKNK//\'','IHH@GMAIL.COM',0);

#
# Structure for table "detalleventa"
#

DROP TABLE IF EXISTS `detalleventa`;
CREATE TABLE `detalleventa` (
  `idProducto` int(11) DEFAULT NULL,
  `idVenta` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  KEY `idVenta` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "detalleventa"
#

INSERT INTO `detalleventa` VALUES (4,1,10),(5,1,10),(4,NULL,NULL),(5,NULL,NULL),(6,NULL,NULL),(5,1,12),(5,1,12),(5,4,12),(1,8,10),(5,8,13),(5,11,6),(1,11,100),(1,14,13),(5,15,53),(1,15,20),(5,15,23),(1,16,12),(4,16,13),(4,17,14),(1,17,13),(1,18,20),(4,18,10),(4,19,23),(1,19,10),(4,20,132),(4,23,1),(4,24,12),(4,24,12),(1,24,12);

#
# Structure for table "producto"
#

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(45) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `procedencia` varchar(45) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "producto"
#

INSERT INTO `producto` VALUES (1,'12345','ladrillo','roble','cbba',10.00,2000,1),(4,'123456789','SDFGHJK','TYUJKL','GHJ',12.57,4567,1),(5,'1445','DED','SSS','DDS',12.23,22,1),(6,'DD','ssss','S','D',12.23,0,1);

#
# Structure for table "usuario"
#

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(20) DEFAULT NULL,
  `apellidos` varchar(20) DEFAULT NULL,
  `email` varchar(35) DEFAULT '2' COMMENT '1 para administrador\\\\n2 para usuario vendedor',
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(35) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1',
  `idRol` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Data for table "usuario"
#

INSERT INTO `usuario` VALUES (1,'Liseth','rok','gggg@gemail.com','lis','202cb962ac59075b964b07152d234b70',1,1),(4,'osmar','rojas','dkd@gmail.com','https://meet.go','827ccb0eea8a706c4c34a16891f84e7b',1,2),(5,'osmar','rojas','oso@gmail.com','oso','202cb962ac59075b964b07152d234b70',1,2),(6,'qQqQQ','Q','Q@gemail.com','Q','f09564c9ca56850d4cd6b3319e541aee',1,2),(7,'q','e','e@gemaail.com','e','e1671797c52e15f763380b45e841ec32',0,2),(8,'q','e','e@gemaail.com','e','e1671797c52e15f763380b45e841ec32',0,2),(9,'e','e','e@gemaail.com','e','e1671797c52e15f763380b45e841ec32',0,2),(10,'kkk','kkk','kkk@gmail.com','uuu','c70fd4260c9eb90bc0ba9d047c068eb8',1,1),(11,'flavio','flavio','flavio@gmail.com','flavio','f76405ac130dac085b2a6249073b213b',1,1);

#
# Structure for table "venta"
#

DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta` (
  `idVenta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `precioTotal` decimal(10,2) DEFAULT '0.00',
  `idCliente` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idVenta`,`idCliente`,`idUsuario`),
  KEY `fk_venta_cliente_idx` (`idCliente`),
  KEY `fk_venta_usuario1_idx` (`idUsuario`),
  CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`),
  CONSTRAINT `fk_venta_usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

#
# Data for table "venta"
#

INSERT INTO `venta` VALUES (1,'2021-10-26 01:00:41',0.00,1,1,1),(2,'2021-10-26 01:48:33',0.00,1,1,1),(3,'2021-10-26 01:57:40',0.00,1,1,NULL),(4,'2021-10-26 01:58:12',0.00,1,1,2),(5,'2021-10-26 02:12:47',0.00,1,1,2),(6,'2021-10-26 02:19:44',0.00,1,1,2),(7,'2021-10-26 02:28:10',0.00,1,1,2),(8,'2021-10-26 02:37:07',0.00,1,1,2),(9,'2021-10-26 11:17:33',0.00,1,1,2),(10,'2021-10-26 11:19:13',0.00,1,1,2),(11,'2021-10-26 11:24:07',0.00,1,11,2),(12,'2021-10-26 12:11:35',0.00,1,11,2),(13,'2021-10-26 16:20:52',0.00,1,11,2),(14,'2021-10-26 16:34:12',0.00,1,11,2),(15,'2021-10-26 16:52:00',99.00,1,11,2),(16,'2021-10-26 19:23:28',0.00,1,1,2),(17,'2021-10-26 19:32:37',25.00,1,1,2),(18,'2021-10-26 19:36:12',325.70,1,1,1),(19,'2021-10-26 22:55:10',389.11,25,1,1),(20,'2021-10-26 22:59:09',0.00,1,1,2),(21,'2021-10-26 22:59:44',0.00,1,1,2),(22,'2021-10-26 22:59:46',0.00,1,1,2),(23,'2021-10-26 23:00:16',0.00,1,1,2),(24,'2021-10-26 23:01:01',421.68,25,1,1),(25,'2021-10-26 23:37:02',0.00,1,1,2),(26,'2021-10-26 23:43:27',0.00,1,1,2),(27,'2021-10-26 23:46:02',0.00,1,1,2),(28,'2021-10-26 23:48:13',0.00,1,1,2),(29,'2021-10-26 23:49:00',0.00,1,1,2),(30,'2021-10-27 00:35:17',0.00,1,1,2),(31,'2021-10-27 01:21:13',0.00,1,1,2);
