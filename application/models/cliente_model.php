<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model {

	public function retornarCliente()
	{
		$this->db->select('*');
		$this->db->from('cliente');
		$this->db->where('estado',0);
		return $this->db->get();
	}
	public function agregarCliente($data)
	{
		$this->db->insert('cliente',$data);	
	}

    public function recuperarCliente($idCliente)
	{
		$this->db->select('*');
		$this->db->from('cliente');
		$this->db->where('idCliente',$idCliente);
		return $this->db->get();
	}
	
	public function modificarCliente($idCliente,$data)
	{
		$this->db->where('idCliente',$idCliente);
		$this->db->update('cliente',$data);
	}
	
	public function eliminarCliente($idCliente,$data)
	{
		$this->db->where('idCliente',$idCliente);
		$this->db->update('cliente',$data);
	}

}