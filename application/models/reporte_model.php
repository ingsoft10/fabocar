<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte_model extends CI_Model {

  public function getVenta()
	{
		$this->db->select('V.idVenta, V.precioTotal, DATE(V.fecha) as fecha, Concat(C.nombres," ",C.apellidos) as cliente, D.cantidad,
                        P.nombre as producto');
		$this->db->from('venta V');
		$this->db->join('cliente C','V.idCliente = C.idCliente');
    $this->db->join('detalleVenta D','D.idVenta = V.idVenta');
    $this->db->join('producto P','P.idProducto = D.idProducto');
		$this->db->where('V.estado',1);
		return $this->db->get();
	}

}
