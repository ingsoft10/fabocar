<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta_model extends CI_Model {

  public function retornarVenta()
	{
		$this->db->select('V.idVenta, V.precioTotal, DATE(V.fecha) as fecha, C.nombres');
		$this->db->from('venta V');
		$this->db->join('cliente C','V.idCliente = C.idCliente');//, C.nombres
		$this->db->where('V.estado',1);
		return $this->db->get();
	}
	public function getVenta($idVenta)
	{
		$this->db->select('V.idVenta, V.fecha, V.precioTotal, V.idCliente, V.idUsuario, C.nombres');
		$this->db->from('venta V');
		$this->db->join('cliente C','V.idCliente = C.idCliente');//, C.nombres
		$this->db->where('V.idVenta',$idVenta);
		return $this->db->get();
	}
	public function getDetalleVenta($idVenta){
		$this->db->select('D.cantidad, P.codigo, P.nombre, P.categoria, P.procedencia, P.precio');
		$this->db->from('detalleventa D');
		$this->db->join('producto P','D.idProducto = P.idProducto');//, C.nombres
		$this->db->where('D.idVenta',$idVenta);
		return $this->db->get();
	}
	public function confirmarVenta($data)
	{
		$idVenta= $data['idVenta'];
		$this->db->WHERE('idVenta',$idVenta);
	    $this->db->UPDATE('venta', array(                 
	   'precioTotal' => $data['precioTotal'],
	   'idCliente' => $data['idCliente'],    
	   'estado' => 1,    
       ));
	}
	public function recuperarIdVenta()
    {
       $this->db->SELECT('MAX(idVenta) AS id');
       $this->db->FROM('venta');
       return $this->db->get();
	   //return 8;
    }

//
	public function insertarVenta()
    {
		$idUsuario=$_SESSION['id'];
		//$idUsuario=1;
		$idCliente = 1;
        $data['idCliente'] = $idCliente;
	 	$data['idUsuario'] = $idUsuario;
		$data['estado'] = 2; 
	 	$this->db->insert('venta',$data);	  
  
    }
	public function insertarDetalleVenta($data){
	    $this->db->insert('detalleventa',$data);
	}
}
