<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * PDF helper
 */
function create_pdf($html, $filename = '')
{
    // need to enable magic quotes for the

    if (version_compare(PHP_VERSION, '5.5.1', '>')) {
        $magic_quotes_enabled = get_magic_quotes_runtime();
        if(!$magic_quotes_enabled)
        {
        	ini_set('magic_quotes_runtime', TRUE);
        }
    }
    $options = new Dompdf\Options();
    $options->set('isRemoteEnabled', TRUE);
    $dompdf = new Dompdf\Dompdf($options);
    //$dompdf = new Dompdf\Dompdf();

    $contxt = stream_context_create([
        'ssl' => [
            'verify_peer' => FALSE,
            'verify_peer_name' => FALSE,
            'allow_self_signed'=> TRUE
        ]
    ]);
    //$dompdf->setHttpContext($contxt);
    //$dompdf = new Dompdf\Dompdf();
    //loadHtml
    $dompdf->loadHtml(str_replace(array("\n", "\r"), '', $html));
    $dompdf->render();
    if (version_compare(PHP_VERSION, '5.5.1', '<')) {
         if ( get_magic_quotes_gpc() )
            $html = stripslashes($html);
        if(!$magic_quotes_enabled)
        {
    		ini_set('magic_quotes_runtime', $magic_quotes_enabled);
    	}
    }

    if($filename != '')
    {
        try{
            $dompdf->stream($filename . '.pdf');
        }
        catch(Exception $e){
            echo "error";
            var_dump($e);
        }
    }
    else
    {
        $dompdf->stream(
          'file.pdf',
          array(
            'Attachment' => 0
          )
        );
        //return $dompdf->output();
    }
}
?>
