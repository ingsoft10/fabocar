
<!-- START Main section-->
<section>
   <!-- START Page content-->
   <section class="main-content">
                        <?php
                                            $bandera=0;?>
            <a  href="<?=base_url()?>index.php/venta/agregarVenta/0" type="submit" class="btn btn-round btn-primary"><i class="fa fa-plus "></i> Registrar nueva venta </a>
     
      <h3>Ventas
         <br>
         <small>Lista</small>
      </h3>

      <!-- START DATATABLE 3-->
      <div class="row">
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">Lista de usuarios |
                  <small>Detalles</small>
               </div>
               <div class="panel-body">
               <table id="datatable3" class="table table-striped table-hover">
                                
                                <thead class="thead-inverse">
                                    <tr>
                                    <th>Nª</th>
                                    <th>NOMBRE DEL CLIENTE</th>
                                    <th>TOTAL IMPORTE/BS</th>
                                    <th>FECHA DE VENTA</th>
                                    <th style="width: 130px">OPCIONES</th>
                                    
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $indice=1;
                                    foreach ($venta->result() as $row) {
                                    ?>
                                        <tr>
                                            <td><?php echo $indice; ?></td>
                                            <td><?php echo $row->nombres; ?></td>
                                            <td><?php echo $row->precioTotal; ?></td>
                                            <td><?php echo $row->fecha; ?></td>
                                            <td>
                                            <div class="btn-group" >
                                            <a href="<?php $idVenta = $row->idVenta; echo base_url();?>index.php/venta/view/<?php echo $idVenta;?>"
                                             class="btn btn-oval btn-primary" data-toggle="modal" data-target="#myModal"> <span class="fa fa-search"></span> </a> 
                                             <a href="<?php echo base_url();?>index.php/venta/recibo/<?php echo $row->idVenta;?>" class="btn btn-oval btn btn-danger">
                                                                <span class="fa fa-print"> Recibo</span>
                                                                 </a> 
                                          </div>
                     
                                            </td>
                                        </tr> 
                                    <?php
                                    $indice++;
                                    }
                                    ?>
                                </tbody>
                            </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END DATATABLE 3-->

   </section>
   <!-- END Page content-->
</section>
<!-- END Main section-->


</section>
<!-- END Main wrapper-->


<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
       <div class="modal-content">
          <div class="modal-header">

          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">

          </div>
       </div>
    </div>
 </div>