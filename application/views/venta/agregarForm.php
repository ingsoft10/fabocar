



<!-- START Main section-->
<section>
   <!-- START Page content-->
   <section class="main-content">
      <a href="<?php echo base_url();?>index.php/venta/confirmarVenta" class="btn btn-primary btn-labeled pull-right" data-toggle="modal" data-target="#myModal">
        <span class="btn-label"><i class="fa fa-plus-circle"></i></span>Confirmar Venta
      </a>
      <h3>SELECCIONAR PRODUCTOS
         <br>
      </h3>
      
      <!-- START DATATABLE 3-->
      <div class="row">
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">Lista de productos |
                  <small>Detalles</small>
               </div>
               <div class="panel-body">
               <table id="datatable3" class="table table-striped table-hover">     
                        <thead class="thead-inverse">
                            <tr>
                             <th style="width: 3%">Nº</th>
                            <th>CODIGO</th>
                            <th>NOMBRE</th> 
                            <th>CATEGORIA</th>                      
                            <th>PROCEDENCIA</th>
                            <th>PRECIO/Bs</th>
                            <th>STOCK</th>
                            <th style="width: 100px">SELECCIONAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $indice=1;
                            foreach ($producto->result() as $row) {
                            ?>
                                <tr>
                                    
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->codigo; ?></td>
                                    <td><?php echo $row->nombre; ?></td>
                                    <td><?php echo $row->categoria; ?></td>
                                    <td><?php echo $row->procedencia; ?></td>
                                    <td><?php echo $row->precio; ?></td>
                                    <td><?php echo $row->cantidad; ?></td>
                                    
                                    <td>
                                       <div class="btn-group">
                                       <a href="<?php echo base_url()?>index.php/venta/agregarProducto/<?php echo $row->idProducto;?>"
                                         class="btn btn-success" data-toggle="modal" data-target="#myModal2">+</a>
                                    
                                       </div>
                                
                                    </td>
                                </tr>
                                    
                            <?php
                            $indice++;
                            }
                            ?>
                        </tbody>
                    </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END DATATABLE 3-->

   </section>
   <!-- END Page content-->
</section>
<!-- END Main section-->


</section>


<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
       <div class="modal-content">
          <div class="modal-header">

          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">

          </div>
       </div>
    </div>
 </div>

 <div id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
       <div class="modal-content">
          <div class="modal-header">

          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">

          </div>
       </div>
    </div>
 </div>

<div id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog">
       <div class="modal-content">
          <div class="modal-header">
             <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
             <h4 id="myModalLabel" class="modal-title">Eliminar Registro</h4>
          </div>
          <div class="modal-body alert alert-danger alert-dismissable ">
            Esta seguro de quere eliminar este registro?
          </div>
          <div class="modal-footer">
             <a href="<?php echo base_url()?>index.php/venta/agregarDetalleVenta/<?php echo $producto->idProducto;?>"
                  class="btn btn-danger btn-labeled">
                  <span class="btn-label"> <i class="fa fa-trash-o"></i></span>Eliminar</a>
             <button type="button" data-dismiss="modal" class="btn btn-primary">Cancelar</button>

          </div>
       </div>
    </div>
 </div>