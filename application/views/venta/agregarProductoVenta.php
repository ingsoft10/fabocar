
<section>
   <!-- START Page content-->
   <section class="main-content">

      <!-- START DATATABLE 3-->
      <div class="row">
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading"> 
                  <small><?php echo $producto->nombre; $bandera = 1; ?></small>
               </div>

               
               <form action="<?php echo base_url();?>index.php/venta/agregarDetalleVenta/<?php echo $producto->idProducto;?>" method="post" role="form" class="mb-lg">
                 <?php if($this->session->flashdata("error")):?>
                         <div class="alert alert-danger">
                           <p> <?php echo $this->session->flashdata("error"); ?></p>
                         </div>
                 <?php endif; ?>

               <div class="panel-body" >
                           <input type="hidden" id="idProducto" name="idProducto" value="<?php echo $producto->idProducto?>" class="form-control">
                           <div class="form-group">
                               
                              <label for="nombre">Cantidad: (*)</label>
                              <input type="text" id="cantidad" name="cantidad" value="1" class="form-control" minlegth="1" maxlength="20" required="" pattern="[0-9]+">
                           </div>
                         
                           <button type="submit" class="btn btn-labeled btn-success pull-left">
                             <span class="btn-label"><i class="fa fa-check-circle"></i></span>Agregar</button>
                    </form>

                    <a href="<?php echo base_url();?>index.php/venta/agregarVenta/<?php echo $bandera?>" class="btn btn-danger btn-labeled pull-right">
                     <span class="btn-label"><i class="fa fa-ban"></i></span>Cancelar
                     </a>


               </div>
            </div>
         </div>
      </div>
      <!-- END DATATABLE 3-->

   </section>
   <!-- END Page content-->
</section>
<!-- END Main section-->


</section>
<!-- END Main wrapper-->
