<!-- START Main section-->
<section>
   <!-- START Page content-->
   <section class="main-content">

      <!-- START DATATABLE 3-->
      <div class="row">
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">Detalle de Venta |
                  <small>precio</small>
               </div>
               <h3>Cliente</h3>
           
               <?php $idCliente = 1;?> 
               <form action="<?php echo base_url();?>index.php/venta/confirmarVentadb" method="post" role="form" class="mb-lg">
                 <select name="idCliente" id="idCliente"><?php
            // or whatever you want
                foreach ($clientes->result() as $row){
         
                ?><option value="<?php echo $row->idCliente; ?>"<?php
                   ?>><?php echo $row->nombres." ".$row->apellidos ; ?></option><?php
              }
           ?></select>
                 <?php if($this->session->flashdata("error")):?>
                         <div class="alert alert-danger">
                           <p> <?php echo $this->session->flashdata("error"); ?></p>
                         </div>
                 <?php endif; ?>
           <div class="col-lg-12">
               <div class="panel-body" >
                   <?php $precioTotal = 0;?>
               <table id="datatable3" class="table table-striped table-hover">     
                        <thead class="thead-inverse">
                            <tr>
                             <th style="width: 3%">Nº</th>
                            <th>nombre</th> 
                            <th>categoria</th>                      
                            <th>procedencia</th>
                            <th>precio/U/Bs.</th>
                            <th>cantidad</th>
                            <th>precio.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $indice=1;
                            foreach ($detalle->result() as $row) {
                            ?>
                                <tr>
                                    
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->nombre; ?></td>
                                    <td><?php echo $row->categoria; ?></td>
                                    <td><?php echo $row->procedencia; ?></td>
                                    <td><?php echo $row->precio; ?></td>
                                    <td><?php echo $row->cantidad; ?></td>
                                    <td><?php $precioProducto = $row->cantidad*$row->precio; echo $precioProducto?></td>
                                     <?php $precioTotal = $precioTotal + $precioProducto?>
                                </tr>
                                    
                            <?php
                            $indice++;
                            }
                            ?>
                        </tbody>
                    </table>
                       <h4>precio total = <?php echo $precioTotal?> bs </h4>
                       <input type="hidden" id="precioTotal" name="precioTotal" value="<?php echo $precioTotal?>" class="form-control">
                        <button type="submit" class="btn btn-labeled btn-success pull-left">
                             <span class="btn-label"><i class="fa fa-check-circle"></i></span>Confirmar</button>
                    </form>

                    <a href="<?php echo base_url();?>index.php/venta/listaVenta/" class="btn btn-danger btn-labeled pull-right">
                     <span class="btn-label"><i class="fa fa-ban"></i></span>Cancelar
                     </a>


               </div>
                        </div> 
            </div>
         </div>
      </div>
      <!-- END DATATABLE 3-->

   </section>
   <!-- END Page content-->
</section>
<!-- END Main section-->


</section>
<!-- END Main wrapper-->