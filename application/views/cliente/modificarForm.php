<section class="main-content">
    <center><section class="content-header">
        <h1>
          MODIFICANDO EL CLIENTE 
        </h1> 
    </section>
    <br>
    <section>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-collapse">
                    <br>
                    <?php
                    foreach ($nombres->result() as $row) {
                    ?>
                    <?php echo form_open_multipart('index.php/cliente/modificardb'); ?>                            
                    <form action="/action_page.php">                                    
                            <input type="hidden" name="idCliente" value="<?php echo $row->idCliente; ?>">                                     
                        <div class="form-group col-md-6">
                            <label for="nombres">Nombres: </label>
                            <input type="text" name="nombres" class="form-control" id="nombres" value="<?php echo $row->nombres; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70" pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" required="" title="Solo puede contener caracteres alfavetico.">
                        </div >        
                        <div class="form-group col-md-6">
                            <label for="apellidos"> Apellidos: </label>
                            <input type="text" name="apellidos" class="form-control" id="apellidos" value="<?php echo $row->apellidos; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="20"pattern="[.A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" title="Solo puede contener caracteres alfanumerico.">
                        </div >      
                        <div class="form-group col-md-6">
                            <label for="telefono">Telefono: </label>
                            <input type="text" name="telefono" class="form-control" id="telefono" value="<?php echo $row->telefono; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70" pattern="[+A-Z0-9 ]+" title="Solo puede contener caracteres alfanumerico.">
                        </div >   
                        <div class="form-group col-md-6">
                            <label for="direccion"> Direcciòn: </label>
                            <input type="text" name="direccion" class="form-control" id="direccion" value="<?php echo $row->direccion; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70">
                        </div >      
                        <div class="form-group col-md-12">
                            <label for="correo"> Correo: </label>
                            <input type="email" name="correo" class="form-control" id="correo" value="<?php echo $row->correo; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70">
                        </div >                                                     
                        <center><div >                                    
                            <button type="submit" class="btn btn-round btn-primary pull-center">Guardar</button>
                            <?php echo form_close(); ?>                                    
                        <a href="<?=base_url()?>index.php/cliente/listaCliente" class="btn btn-round btn-danger" type="submit">Cancelar</a>
                        </div >                                           
                        <?php
                        }
                        ?>
                    </form>                            
                </div>
            </div>
        </div>     
    </section>
</section>
        
    
