<section class="main-content">
    <center><section class="content-header">
        <h3>
        REGISTRO DE UN NUEVO CLIENTE
        </h3>
    </section>
    <br>
    <section>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-collapse">
                <br>
                <?php if($this->session->flashdata("error")):?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                </div>
                <?php endif;?>
                 <?php echo form_open_multipart('index.php/cliente/agregardb'); ?>                            
                 <div><!-- ction="<vv?php echo base_url();?>index.php/cliente/agregardb" -->                             
                    <form method="POST">                                    
                        <div class="form-group col-md-6">
                            <label for="nombres">Nombres: </label>
                            <input type="text" name="nombres" placeholder="Ingrese nombress.."class="form-control" id="nombres" value="<?php echo set_value("nombres");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70" pattern="[.A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" required="" title="Solo puede contener caracteres alfabetico.">
                        </div >
                        <div class='form-group col-md-6'>
                            <label for="apellidos">Apellidos: </label>
                            <input type="text" name="apellidos" placeholder="Descripción de la apellidos.."class="form-control" id="apellidos" value="<?php echo set_value("apellidos");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70" pattern="[,.A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" title="Solo puede contener caracteres alfabetico." >
                            <?php echo form_error("apellidos","<span class='help-block'>","</span>");?>
                        </div > 
                        <div class="form-group col-md-6">
                            <label for="telefono">Telefono: </label>
                            <input type="text" name="telefono" placeholder="Ingrese .."class="form-control"  value="<?php echo set_value("telefono");?>" onkeyup = "this.value=this.value.toUpperCase()" pattern="[+0-9 ]+" title="Solo puede contener caracteres numéricos." >
                        </div >
                        <div class="form-group col-md-6">
                            <label for="direccion">Dirección: </label>
                            <input type="text" name="direccion" placeholder="Ingrese."class="form-control"  value="<?php echo set_value("direccion");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70">
                        </div >
                        <div class="form-group col-md-12">
                            <label for="correo">Correo Electronico: </label>
                            <input type="email" name="correo" placeholder="Ingrese ."class="form-control"  value="<?php echo set_value("correo");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70">
                        </div >
                        <br>              
                        <center><div>
                            <button type="submit" class="btn btn-round btn-primary">Guardar</button>
                            <?php echo form_close(); ?>
                            <a href="<?=base_url()?>index.php/cliente/listaCliente" class="btn btn-round btn-danger" >Cancelar</a>
                        </div > 
                        <br> 
                    </form>
                </div>
            </div>       
        </div>  
    </section>
</section>    
