<section class="main-content">
    <section class="content-header">
        <br>
        <h2 class="text-center">
        LISTA DE CLIENTES
        </h2>
    </section>
    <br>
    <center><section class="content-header">
            <a  href="<?=base_url()?>index.php/cliente/agregar" type="submit" class="btn btn-round btn-primary"><i class="fa fa-plus "></i> Nuevo Cliente </a>
    </section>
    <br>
    </div>


          <!-- START chart-->
    <div class="row">
       <div class="col-lg-12">
          <div class="panel panel-default">
             <div class="panel-collapse">
                <div class="panel-body">
                    
                   
                <table id="datatable3" class="table table-striped table-hover">
                        
                        <thead class="thead-inverse">
                            <tr>
                            <th>#</th>
                            <th>NOMBRE</th>
                            <th>APELLIDOS</th>
                            <th>TELEFONO</th>
                            <th>DIRECCION</th>
                            <th>CORREO ELECT</th>
                            <th class="sort-alpha">OPCIONES</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $indice=1;
                            foreach ($cliente->result() as $row) {
                            ?>
                                <tr>
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->nombres; ?></td>
                                    <td><?php echo $row->apellidos; ?></td>
                                    <td><?php echo $row->telefono; ?></td>
                                    <td><?php echo $row->direccion; ?></td>
                                    <td><?php echo $row->correo; ?></td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('index.php/cliente/modificar'); ?>
                                            <input type="hidden" name="idCliente" value="<?php echo $row->idCliente; ?>">
                                            <button class="btn btn-oval btn-warning" type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                        
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('index.php/cliente/eliminardb'); ?>
                                                <input type="hidden" name="idCliente" value="<?php echo $row->idCliente; ?>">
                                                <button type="submit" class="btn btn-oval btn-danger"><i class="fa fa-trash-o"></i></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </td>
                                </tr>
                                    
                            <?php
                            $indice++;
                            }
                            ?>
                        </tbody>
                    </table>
                            


                    
                  
                </div>
             </div>
          </div>
       </div>
    </div>







</div>