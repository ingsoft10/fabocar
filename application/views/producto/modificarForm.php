<section class="main-content">
            <section class="content-header">
                <center><h1>
                  MODIFICANDO DATOS DE PRODUCTO
                </h1>
            </section>
            <section class="content">
                <div class="col-md-12 section-bbb"><div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-collapse">                <?php
                                foreach ($codigo->result() as $row) {
                                ?>
                                <?php echo form_open_multipart('index.php/producto/modificardb'); ?>
                            
                                <form>
                                    <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">
                                     <div class="form-group col-md-6">
                                        <label for="codigo">Codigo: </label>
                                        <input type="text" name="codigo" placeholder="Ingrese codigo.."class="form-control" id="codigo" value="<?php echo $row->codigo; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class="form-group col-md-6">
                                        <label for="nombre">Nombre: </label>
                                        <input type="text" name="nombre" placeholder="Ingrese nombre del producto.."class="form-control" id="nombre"value="<?php echo $row->nombre; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class='form-group col-md-6'>
                                        <label for="categoria">Categoria: </label>
                                        <input type="text" name="categoria" placeholder="categoria.."class="form-control" id="categoria" value="<?php echo $row->categoria; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" title="Solo puede contener caracteres numéricos." >
                                   </div > 
                                    <div class="form-group col-md-6">
                                        <label for="procedencia">Procedencia: </label>
                                        <input type="text" name="procedencia" placeholder="Ingrese procedencia .."class="form-control"  value="<?php echo $row->procedencia; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class="form-group col-md-6">
                                        <label for="precio">Precio: </label>
                                        <input type="text" name="precio" placeholder="Ingrese precio."class="form-control"  value="<?php echo $row->precio; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[,.0-9 ]+"  title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class="form-group col-md-6">
                                        <label for="cantidad">Cantidad: </label>
                                        <input type="text" name="cantidad" placeholder="Ingrese cantidad ."class="form-control"  value="<?php echo $row->cantidad; ?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+"  title="Solo puede contener caracteres alfavetico.">
                                    </div>
                                    <br>
                                    <center><div>
                                        <button type="submit" class="btn btn-round btn-primary">Registrar</button>
                                        <?php echo form_close(); ?>

                                        <a href="<?=base_url()?>index.php/producto/listaProducto" class="btn btn-round btn-danger" type="submit">Cancelar</a>
                                    </div >
                                    <br> 
                                    <?php
                                    }
                                    ?>
                                 </form>
                                 
                        </div>
                    </div>
                </div>
                             
                </div>    
            </section>
                     
            </div>
</section>