<section class="main-content">
            <center><section class="content-header">
                <h3>
                REGISTRO DE UN NUEVO PRODUCTO
                </h3>
            </section>
            <br>
            <section>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-collapse">
                    <br>

                            <?php if($this->session->flashdata("error")):?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                            </div>
                            <?php endif;?>
                             <?php echo form_open_multipart('index.php/producto/agregardb'); ?>
                            
                             <div>
                             
                                <form method="POST">
                                
                                    <div class="form-group col-md-6">
                                        <label for="codigo">Codigo: </label>
                                        <input type="text" name="codigo" placeholder="Ingrese codigo.."class="form-control" id="codigo" value="<?php echo set_value("codigo");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class="form-group col-md-6">
                                        <label for="nombre">Nombre: </label>
                                        <input type="text" name="nombre" placeholder="Ingrese nombre del producto.."class="form-control" id="nombre" value="<?php echo set_value("nombre");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[.A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class='form-group col-md-6'>
                                        <label for="categoria">Categoria: </label>
                                        <input type="text" name="categoria" placeholder="categoria.."class="form-control" id="categoria" value="<?php echo set_value("categoria");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" title="Solo puede contener caracteres numéricos." >
                                   </div > 
                                    <div class="form-group col-md-6">
                                        <label for="procedencia">Procedencia: </label>
                                        <input type="text" name="procedencia" placeholder="Ingrese procedencia .."class="form-control"  value="<?php echo set_value("procedencia");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[A-Za-zñÑáéíóúÁÉÍÓÚ0-9 ]+" title="Solo puede contener caracteres alfavetico.">
                                    </div >
                                    <div class="form-group col-md-6">
                                        <label for="precio">Precio: </label>
                                        <input type="text" name="precio" placeholder="Ingrese precio."class="form-control"  value="<?php echo set_value("precio");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[.,0-9 ]+"  title="Solo puede contener caracteres numerico.">
                                    </div >
                                    <div class="form-group col-md-6">
                                        <label for="cantidad">Cantidad: </label>
                                        <input type="text" name="cantidad" placeholder="Ingrese cantidad ."class="form-control"  value="<?php echo set_value("cantidad");?>" onkeyup = "this.value=this.value.toUpperCase()" maxlength="70"pattern="[,.0-9 ]+"  title="Solo puede contener caracteres numerico.">
                                    </div >
                                    
                                    
                                    <br>              
                                    <center><div>
                
                                        <button type="submit" class="btn btn-round btn-primary">Guardar</button>
                                        <?php echo form_close(); ?>
                                        <a href="<?=base_url()?>index.php/producto/listaProducto" class="btn btn-round btn-danger" >Cancelar</a>
                                    </div > 
                                    <br> 
                                    
                                </form>
                            </div>
                    </div>       
                </div>  
            </section>
        </div>
    </div>
</section>  
