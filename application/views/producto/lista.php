<section class="main-content">
    <section class="content-header">
        <br>
        <h2 class="text-center">
        LISTADO DE PRODUCTOS
        </h2>
    </section>
    <br>
    <center><section class="content-header">
            <a  href="<?=base_url()?>index.php/producto/agregar" type="submit" class="btn btn-round btn-primary"><i class="fa fa-plus "></i> Nuevo Producto </a>
    </section>
    <br>
    </div>


          <!-- START chart-->
    <div class="row">
       <div class="col-lg-12">
          <div class="panel panel-default">
             <div class="panel-collapse">
                <div class="panel-body">
                    
                    <table id="datatable3" class="table table-striped table-hover">     
                        <thead class="thead-inverse">
                            <tr>
                             <th style="width: 3%">Nº</th>
                            <th>CODIGO</th>
                            <th>NOMBRE</th> 
                            <th>CATEGORIA</th>                      
                            <th>PROCEDENCIA</th>
                            <th>PRECIO/Bs</th>
                            <th>STOCK</th>
                            <th style="width: 100px">OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $indice=1;
                            foreach ($producto->result() as $row) {
                            ?>
                                <tr>
                                    
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->codigo; ?></td>
                                    <td><?php echo $row->nombre; ?></td>
                                    <td><?php echo $row->categoria; ?></td>
                                    <td><?php echo $row->procedencia; ?></td>
                                    <td><?php echo $row->precio; ?></td>
                                    <td><?php echo $row->cantidad; ?></td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('index.php/producto/modificar'); ?>
                                            <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">
                                            <button class="btn btn-warning btn btn-round" type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                        
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('index.php/producto/eliminardb'); ?>
                                                <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">
                                                <button type="submit" class="btn btn-danger btn btn-round  btn-btnEliminar"><i class="fa fa-bitbucket"></i></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </td>
                                </tr>
                                    
                            <?php
                            $indice++;
                            }
                            ?>
                        </tbody>
                    </table>
                            


                    
                  
                </div>
             </div>
          </div>
       </div>
    </div>







</div>


