      <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
         <nav class="sidebar">
            <ul class="nav">
               <!-- START user info-->
               <li>
                  <div data-toggle="collapse-next" class="item user-block has-submenu">
                     <!-- User picture-->
                     <div class="user-block-picture">
                        <img src="<?php echo base_url();?>assets/img/page/logofabocar.jpg" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                        <!-- Status when collapsed-->
                        <div class="user-block-status">
                           <div class="point point-success point-lg"></div>
                        </div>
                     </div>
                     <!-- Name and Role-->
                     <div class="user-block-info">
                        <span class="user-block-name item-text">Bienvenido, <?php echo $this->session->userdata("nombre"); ?></span>
                        <span class="user-block-role"></span>
                        <!-- START Dropdown to change status-->
                        <div class="btn-group user-block-status">
                           <button type="button" data-toggle="dropdown" data-play="fadeIn" data-duration="0.2" class="btn btn-xs dropdown-toggle">
                        </div>
                     </div>
                  </div>
               <li>
                  <a href="<?=base_url()?>index.php/venta/listaVenta" title="Data Table" data-toggle="" class="no-submenu">
                     <em class="fa fa-money"></em>
                     <span class="item-text">Ventas</span>
                  </a>
               </li>
               <li>
                  <a href="<?=base_url()?>index.php/cliente/listaCliente" title="Data Table" data-toggle="" class="no-submenu">
                     <em class="fa fa-bar-chart-o"></em>
                     <span class="item-text">Clientes</span>
                  </a>
               </li>

               <?php
                  if($this->session->userdata('rol') == 1) {
               ?>

               <li>
                  <a href="<?php echo base_url();?>/#" title="Tables" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-sitemap"></em>
                     <span class="item-text">Administracion</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?=base_url()?>index.php/producto/listaProducto" title="Data Table" data-toggle="" class="no-submenu">
                           <span class="item-text">Productos</span>
                        </a>
                     </li>
                  </ul>
                  <ul class="nav collapse ">
                     <li>
                        <a  href="<?=base_url()?>index.php/Administracion/usuarios"  title="Data Table" data-toggle="" class="no-submenu">
                           <span class="item-text">Usuarios</span>
                           <div class="label label-primary pull-right">!</div>
                        </a>
                     </li>
                  </ul>
                  <!-- END SubMenu item-->
               </li>
               <?php
                  }
               ?>
                <li>
                  <a href="<?php echo base_url();?>/#" title="Data Table" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-bar-chart-o"></em>
                     <span class="item-text">Reporte</span>
                  </a>
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url();?>index.php/reporte" title="Data Table" data-toggle="" class="no-submenu">
                           <span class="item-text">Ventas</span>
                        </a>
                     </li>
                  </ul>
                </li>
               <!-- END Menu-->
               <!-- Sidebar footer    -->
               <li class="nav-footer">
                  <div class="nav-footer-divider"></div>
               </li>
            </ul>
         </nav>
         <!-- END Sidebar (left)-->
      </aside>
      <!-- End aside-->
