<!-- START Main section-->
<section>
   <!-- START Page content-->
   <section class="main-content">
      <h3>Reporte
         <br>
         <small>Lista</small>
      </h3>

      <!-- START DATATABLE 3-->
      <div class="row">
        <!--  form action="</?php echo base_url();?>index.php/reporte/pdf"  -->
    <!--     <form action="</?php echo base_url();?>index.php/reporte/exportarPDF"-->
        <form action="<?php echo base_url();?>index.php/reporte/exportarPDF"
           method="POST" target="_blank">
        <div class="col-sm-6 col-md-3">
           <button type="submit" class="btn btn-primary"><i class="fa fa-file-pdf-o"> Exportar</i></button>
         </div>
       </form>
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">Reporte de Ventas |
                  <small>Detalles</small>
               </div>
               <div class="panel-body">
                  <table id="datatable3" class="table table-striped table-hover display nowrap">
                  <!--table id="datatable3" class="table table-striped table-hover"-->
                     <thead>
                        <tr>
                          <th>#</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Cliente</th>
                          <th>Fecha de venta</th>
                          <th>Total Importe</th>
                          <!--th class="sort-numeric">Nombre de Usuario</th-->
                        </tr>
                     </thead>
                     <tbody>
                       <?php  if(!empty($ventas)):?>
                         <?php $cont = 1; ?>
                         <?php foreach ($ventas->result() as $venta): ?>
                               <tr class="gradeX">
                                  <td><?php echo $cont; ?></td>
                                  <td><?php echo $venta->producto; ?></td>
                                  <td><?php echo $venta->cantidad; ?></td>
                                  <td><?php echo $venta->cliente; ?></td>
                                  <td><?php echo $venta->fecha; ?></td>
                                  <td><?php echo $venta->precioTotal; ?></td>
                              </tr>
                              <?php $cont++; ?>
                        <?php endforeach; ?>
                      <?php endif; ?>
                        <tfoot>
                          <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Cliente</th>
                            <th>fecha de venta</th>
                            <th>Total Importe</th>
                            <!--th class="sort-numeric">Nombre de Usuario</th-->
                          </tr>
                        </tfoot>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END DATATABLE 3-->

   </section>
   <!-- END Page content-->
</section>
<!-- END Main section-->


</section>
<!-- END Main wrapper-->


 <!-- START modal-->

  <!-- END modal-->
