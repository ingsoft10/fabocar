<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <title>Reporte</title>

<style type="text/css">
  .small-box>.inner {
      padding: 10px;
  }
  .small-box .icon {
      color: rgba(0,0,0,.15);
      z-index: 0;
  }
  #tabla {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  #tabla td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
  }

  #tabla th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
  }

</style>
</head>
<body>

   <div class="main-content">

      <h3>Reporte de Venta </h3>
      <hr/>
       <div>
        FFFFFF
      </div>
      <hr/>

      <!--div class="row">
          <div class="col-sm-6 col-md-3">
            <label>Estudiante : </?php echo $estudiante ?></label>
          </div>
          <div class="col-sm-6 col-md-3">
            <label>Desde : </?php echo $desde ?></label>
          </div>
          <div class="col-sm-6 col-md-3">
            <label>Hasta : </?php echo $hasta ?></label>

          </div>

          <div class="col-sm-6 col-md-3">
            <label>Concepto de Pago : </?php echo $concepto ?></label>

          </div>


      </div-->
      <hr/>
      <div class="row">
         <div class="col-lg-12">
            <div class="panel panel-default">
               <div class="panel-heading">Ventas
               </div>
               <div class="panel-body">
                 <table id="tabla" class="table table-striped table-hover display nowrap">
                 <!--table id="datatable3" class="table table-striped table-hover"-->
                    <thead>
                       <tr>
                         <th>#</th>
                         <th>Producto</th>
                         <th>Cantidad</th>
                         <th>Cliente</th>
                         <th>Fecha de venta</th>
                         <th>Total Importe</th>
                         <!--th class="sort-numeric">Nombre de Usuario</th-->
                       </tr>
                    </thead>
                    <tbody>
                      <?php
                      $suma_total = 0;
                      if(!empty($ventas)):?>
                        <?php $cont = 1; ?>
                        <?php foreach ($ventas->result() as $venta):
                          $suma_total+=$venta->precioTotal; ?>

                              <tr class="gradeX">
                                 <td><?php echo $cont; ?></td>
                                 <td><?php echo $venta->producto; ?></td>
                                 <td><?php echo $venta->cantidad; ?></td>
                                 <td><?php echo $venta->cliente; ?></td>
                                 <td><?php echo $venta->fecha; ?></td>
                                 <td><?php echo $venta->precioTotal; ?></td>
                             </tr>
                             <?php $cont++; ?>
                       <?php endforeach; ?>
                     <?php endif; ?>
                      <tfoot>
                         <tr>
                           <th colspan="4"></th>                          
                           <th>Total</th>
                           <th><?php echo number_format($suma_total,2,'.',','); ?></th>
                           <th></th>

                         </tr>
                      </tfoot>
                    </tbody>
                 </table>
               </div>
            </div>
         </div>
      </div>


   </div>


</body>


</html>
