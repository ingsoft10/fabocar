<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {
	public function __construct(){
		parent::__construct();
	
	}

	public function listaProducto()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$data['producto']=$this->producto_model->retornarProducto();
		$this->load->view('producto/lista',$data);
		$this->load->view('layouts/footer');
	}
 
	public function agregar()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('producto/agregarForm');
		$this->load->view('layouts/footer');
	}

	public function agregardb()
	{
		$codigo=$_POST['codigo'];
		$data['codigo']=$codigo;

		$nombre=$_POST['nombre'];
		$data['nombre']=$nombre;

        $categoria=$_POST['categoria'];
		$data['categoria']=$categoria;

		$procedencia=$_POST['procedencia'];
		$data['procedencia']=$procedencia;
        
		$precio=$_POST['precio'];
		$data['precio']=$precio;
        
		$cantidad=$_POST['cantidad'];
		$data['cantidad']=$cantidad;

		$data['estado']=1;
		
		$resultado=$this->producto_model->agregarProducto($data);
		$data['resultado']=$resultado;
		redirect("index.php/producto/listaProducto",'refresh');
	}

	public function modificar()
	{
		$idProducto=$_POST['idProducto'];
		$data['codigo']=$this->producto_model->recuperarProducto($idProducto);
		$data['nombre']=$this->producto_model->recuperarProducto($idProducto);
		$data['categoria']=$this->producto_model->recuperarProducto($idProducto);
		$data['procedencia']=$this->producto_model->recuperarProducto($idProducto);
		$data['precio']=$this->producto_model->recuperarProducto($idProducto);
		$data['cantidad']=$this->producto_model->recuperarProducto($idProducto);

		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('producto/modificarForm',$data);
		$this->load->view('layouts/footer');

    }
	
	public function modificardb()
	{
		$idProducto=$_POST['idProducto'];

		$codigo=$_POST['codigo'];
		$data['codigo']=$codigo;
		$nombre=$_POST['nombre'];
		$data['nombre']=$nombre; 
		$categoria=$_POST['categoria'];
		$data['categoria']=$categoria; 
		$procedencia=$_POST['procedencia'];
		$data['procedencia']=$procedencia; 
		$precio=$_POST['precio'];
		$data['precio']=$precio;
		$cantidad=$_POST['cantidad'];
		$data['cantidad']=$cantidad;
		$data['estado']=1;
		
		$resultado=$this->producto_model->modificarProducto($idProducto,$data);
		$data['resultado']=$resultado;
		redirect("index.php/producto/listaProducto",'refresh');	
	}
	public function eliminardb()
	{
		$idProducto=$_POST['idProducto'];
		$data['estado']=0;
		$resultado=$this->producto_model->eliminarProducto($idProducto, $data);
		$data['resultado']=$resultado;
		$this->listaProducto();
	
    }	

		
}