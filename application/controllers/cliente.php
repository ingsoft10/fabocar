<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function listaCliente()
	{
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
        $data['cliente']=$this->cliente_model->retornarCliente();
        $this->load->view('cliente/lista',$data);
		$this->load->view('layouts/footer');
	}
    public function agregar()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('cliente/agregarForm');
		$this->load->view('layouts/footer');
	}

    public function agregardb()
	{
		$nombres=$_POST['nombres'];
		$data['nombres']=$nombres;
		$apellidos=$_POST['apellidos'];
		$data['apellidos']=$apellidos;
        $telefono=$_POST['telefono'];
		$data['telefono']=$telefono;
        $direccion=$_POST['direccion'];
		$data['direccion']=$direccion;
        $correo=$_POST['correo'];
		$data['correo']=$correo;
		$data['estado']=0;
		$resultado=$this->cliente_model->agregarCliente($data);
		$data['resultado']=$resultado;
		redirect("index.php/cliente/listaCliente",'refresh');
	}

	public function modificar()
	{
		$idCliente=$_POST['idCliente'];
		$data['nombres']=$this->cliente_model->recuperarCliente($idCliente);
		$data['apellidos']=$this->cliente_model->recuperarCliente($idCliente);
		$data['telefono']=$this->cliente_model->recuperarCliente($idCliente);
		$data['direccion']=$this->cliente_model->recuperarCliente($idCliente);
		$data['correo']=$this->cliente_model->recuperarCliente($idCliente);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('cliente/modificarForm',$data);
		$this->load->view('layouts/footer');
    }

	public function modificardb()
	{
		$idCliente=$_POST['idCliente'];
		$nombres=$_POST['nombres'];
		$data['nombres']=$nombres;
		$apellidos=$_POST['apellidos'];
		$data['apellidos']=$apellidos;
        $telefono=$_POST['telefono'];
		$data['telefono']=$telefono;
        $direccion=$_POST['direccion'];
		$data['direccion']=$direccion;
        $correo=$_POST['correo'];
		$data['correo']=$correo;
		$data['estado']=0;
		$resultado=$this->cliente_model->modificarCliente($idCliente,$data);
		$data['resultado']=$resultado;
		redirect("index.php/cliente/listaCliente",'refresh');	
	}

	public function eliminardb()
	{
		$idCliente=$_POST['idCliente'];
		$data['estado']=1;
		$resultado=$this->cliente_model->eliminarCliente($idCliente, $data);
		$data['resultado']=$resultado;
		$this->listaCliente();
	
    }	
}


