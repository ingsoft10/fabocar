<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model("Reporte_model");
  }

	public function index()
	{
    $data = array (
      'ventas' => $this->Reporte_model->getVenta(),
    );
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('reporte/index', $data);
		$this->load->view('layouts/footer');
  }

  public function exportarPDF()
	{
      $this->load->helper(array('dompdf', 'file'));
      $data = array (
        'ventas' => $this->Reporte_model->getVenta(),
      );
      $html = $this->load->view("reporte/pdf", $data, TRUE);
      //echo $html;
      $ruta = 'pdfs/';
      if(!is_dir($ruta)){
        mkdir($ruta,0755);
      }
      $filename = sys_get_temp_dir() . '/' . 'Reporte_pagos_' . time() . '.pdf';
      //$filename = $ruta. 'Reporte_pagos_' . time() . '.pdf';
      //echo $filename;
      //exit();
      create_pdf($html,$filename);
  }

  public function pdf()
	{
    $data = array (
      'ventas' => $this->Reporte_model->getVenta(),
    );
		$this->load->library('Mypdf');
		$this->mypdf->generate('reporte/pdf', $data, 'Reporte_de_ventas', 'letter', 'Portrait');

	}
}
