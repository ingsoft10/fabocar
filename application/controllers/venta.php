<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
    public function listaVenta()
	{
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
        $data['venta']=$this->venta_model->retornarVenta();
        $this->load->view('venta/lista', $data);
		$this->load->view('layouts/footer');
	}

	public function agregarVenta($bandera)
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$data['producto']=$this->producto_model->retornarProducto();
		$data['clientes']=$this->cliente_model->retornarCliente();
		$this->load->view('venta/agregarForm',$data);
		$this->load->view('layouts/footer');
		if ($bandera<1) {
			$this->venta_model->insertarVenta();
		 }
	}
	public function agregarProducto($idProducto)
    {
      $data['producto'] = $this->producto_model->getProducto($idProducto); 
	  $this->load->view('venta/agregarProductoVenta',$data);
    }
	public function agregarDetalleVenta($idProducto){
		$dato['idProducto'] = $idProducto;
		$idVenta=$this->venta_model->recuperarIdVenta();
		$idVe = 0;
        foreach ($idVenta->result() as $row) {
                 $idVe=$row->id;
        }
		$dato['idVenta'] = $idVe;
		$dato['cantidad'] = $this->input->post("cantidad");
        $this->venta_model->insertarDetalleVenta($dato);
		redirect(base_url()."index.php/venta/agregarVenta/1");
	
	}
	public function confirmarVenta(){
		$idVenta=$this->venta_model->recuperarIdVenta();
		$idVe = 0;
        foreach ($idVenta->result() as $row) {
                 $idVe=$row->id;
        }
		$data['clientes']=$this->cliente_model->retornarCliente();
		$data['venta'] = $this->venta_model->getVenta($idVe);
		$data['detalle'] = $this->venta_model->getDetalleVenta($idVe);
		$this->load->view('venta/confirmarVenta',$data);
	}
	public function confirmarVentadb(){
		$idVenta=$this->venta_model->recuperarIdVenta();
		$idVe = 0;
        foreach ($idVenta->result() as $row) {
                 $idVe=$row->id;
        }
		$data['idVenta'] = $idVe;
		$data['idCliente'] = $_POST['idCliente'];
		$data['precioTotal'] = $this->input->post("precioTotal");
		$this->venta_model->confirmarVenta($data);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
        $data['venta']=$this->venta_model->retornarVenta();
        $this->load->view('venta/lista', $data);
		$this->load->view('layouts/footer');
	}
	public function view($idVenta)
    {
		//$idVenta = 8;
		$data['clientes']=$this->cliente_model->retornarCliente();
		$data['venta'] = $this->venta_model->getVenta($idVenta);
		$data['detalle'] = $this->venta_model->getDetalleVenta($idVenta);
    	$this->load->view('venta/visualizarVenta',$data);
    }

	public function recibo($idVenta)
	{
		$data['clientes']=$this->cliente_model->retornarCliente();
		$data['venta'] = $this->venta_model->getVenta($idVenta);
		$data['detalle'] = $this->venta_model->getDetalleVenta($idVenta);
		$this->load->library('Mypdf');
		$this->mypdf->generate('venta/recibo', $data, 'Recibo', 'A4', 'landscape');
       
	}
}