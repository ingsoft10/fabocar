<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	  public function __construct(){
	 	parent::__construct();
		$this->load->model("Auth_model");
		}

	public function index()
	{
		if($this->session->userdata("login"))
		{
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside');
			$this->load->view('layouts/footer');
			//redirect(base_url()."index.php/cliente/listaCliente");
		}else{
			$this->load->view("login");
		}

	}

	public function login(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$res= $this->Auth_model->login($username, md5($password));

		if(!$res)
		{
			$this->session->set_flashdata("error","El usuario y/o contraseña son incorrectos");
			redirect(base_url());
		}else {
					$data = array(
					'id' =>$res->idUsuario,
				 	'nombre'=>$res->nombres,
					'rol'=>$res->idRol,
					'login'=>TRUE
					);
					$this->session->set_userdata($data);
					//redirect(base_url()."dashboard");
					$this->load->view('layouts/header');
					$this->load->view('layouts/aside');
					$this->load->view('layouts/footer');
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}


}
